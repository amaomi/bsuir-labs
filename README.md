<div align="center"><h2>
 🎓 Белорусский государственный университет информатики и радиоэлектроники <br> 
 Факультет компьютерных систем и сетей <br>
 Специальность "Вычислительные машины, системы и сети" <br>
 2020-2024 г.
<h2></div>

#### 📚 4 семестр

- [СПОВМ - Системное программное обеспечение вычислительных машин <br>
  <code>Linux</code> <code>Concurrent Programming</code> <code>IPC</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/SPOVM)

- [КПиЯП - Конструирование программ и языки программирования <br>
  <code>Assembly</code> <code>NASM</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/KPiYAP-Assembly)

- [АПК - Архитектура персональных компьютеров <br>
  <code>Computer Architecture</code> <code>DOS</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/APK)

- [КПП - Кросс-платформенное программирование <br>
  <code>Java</code> <code>Spring</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/KPP)

#### 📚 3 семестр

- [КПиЯП - Конструирование программ и языки программирования <br>
  <code>C++</code> <code>OOP</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/KPiYAP-Cpp)

#### 📚 2 семестр

- [ОАиП - Основы алгоритмизации и программирования <br>
  <code>C</code> <code>Data Structures</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/OAiP/sem2)

#### 📚 1 семестр

- [ОАиП - Основы алгоритмизации и программирования <br>
  <code>C</code> <code>Algorithms</code>](https://gitlab.com/amaomi/bsuir-labs/-/tree/main/OAiP/sem1)
